<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Client extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get(){


        //$tb_client = $this->M_client->selectBymacAddress('02:00:00:00:00:00')->result();
        $macAddress= $this->get('macAddress');
        $tb_user = $this->M_user->selectBymacAddress($macAddress)->row_array();

        $tb_client = $this->M_client->selectByIdUser($tb_user['id'])->result();


        if($tb_client){
            $this->response($tb_client, 200);
        }else{
            $data = array(
                'id_user' => "",
                'luas_tanah' => "",
                'luas_bangunan' => "",
                'lantai_rumah' => "",
                'jumlah_kamar' => "",
                'pendidikan' => "",
                'rumah_sakit' => "",
                'kantor_polisi' => "",
                'pemadam_kebakaran' => "",
                'harga_rumah' => "",
            );
            $this->response(array($data), 200);
            //$this->response(array('status'=> 'fail', 502));
        }

        //$this->response($tb_client, 200);
    }

    function index_post(){

        $tb_user = $this->M_user->selectBymacAddress($this->post('macAddress'))->row_array();
        if($tb_user != NULL){
            $tb_client = $this->M_client->selectByIdUser($tb_user['id'])->row_array();
            $user_id = $tb_user['id'];
        }else{
            $input['macAddress'] = $this->post('macAddress');
            $user_id = $this->M_user->insert($input);        
            $tb_client = $this->M_client->selectByIdUser($user_id)->row_array();
        }



        $data= array(
            'id_user' => $user_id,
            //'macAddress' => $this->post('macAddress'),
            'luas_tanah' => $this->post('luas_tanah'),
            'luas_bangunan' => $this->post('luas_bangunan'),
            'lantai_rumah' => $this->post('lantai_rumah'),
            'jumlah_kamar' => $this->post('jumlah_kamar'),
            'pendidikan' => $this->post('pendidikan'),
            'rumah_sakit' => $this->post('rumah_sakit'),
            'kantor_polisi' => $this->post('kantor_polisi'),
            'pemadam_kebakaran' => $this->post('pemadam_kebakaran'),
            'harga_rumah' => $this->post('harga_rumah')
        );

            //var_dump($data);
            //$insert = $this->M_client->insert($data);                 
       

        $hasil = (int)$data['luas_tanah']  + (int)$data['luas_bangunan'] + (int)$data['lantai_rumah'] + (int)$data['jumlah_kamar'] + (int)$data['pendidikan'] + (int)$data['rumah_sakit'] + (int)$data['kantor_polisi'] + (int)$data['pemadam_kebakaran'] + (int)$data['harga_rumah'];
        $komputasi = $this->M_komputasi->selectAll()->result_array();
        //var_dump($komputasi);\
        $terbesar = 0;
        foreach ($komputasi as $key) {
            $luas_tanah = $key['luas_tanah'] * ($data['luas_tanah']/$hasil);
            $luas_bangunan = $key['luas_bangunan'] * ($data['luas_bangunan']/$hasil);
            $lantai_rumah = $key['lantai_rumah'] * ($data['lantai_rumah']/$hasil);
            $jumlah_kamar = $key['jumlah_kamar'] * ($data['jumlah_kamar']/$hasil);
            $pendidikan = $key['pendidikan'] * ($data['pendidikan']/$hasil);
            $rumah_sakit = $key['rumah_sakit'] * ($data['rumah_sakit']/$hasil);
            $kantor_polisi = $key['kantor_polisi'] * ($data['kantor_polisi']/$hasil);
            $pemadam_kebakaran = $key['pemadam_kebakaran'] * ($data['pemadam_kebakaran']/$hasil);
            $harga_rumah = $key['harga_rumah'] * ($data['harga_rumah']/$hasil);

            $jumlah = $luas_tanah + $luas_bangunan + $lantai_rumah + $jumlah_kamar + $pendidikan + $rumah_sakit + $kantor_polisi + $pemadam_kebakaran + $harga_rumah;

            if($jumlah >= $terbesar){
                $terbesar = $jumlah;
                $tb_hasil = $this->M_hasil->selectByIdUser($user_id)->row_array();
                //var_dump($tb_hasil);
                if($tb_hasil){
                    $input['id_rumah'] = $key['id_rumah'];
                    $this->M_hasil->update($tb_hasil['id'], $input);
                }else{
                    $input['id_user'] = $user_id;
                    $input['id_rumah'] = $key['id_rumah'];
                    $this->M_hasil->insert($input);                    
                }

            }
        }

        if($tb_client != NULL){
            $cek = $this->M_client->update($tb_client['id'], $data);
        }else{
            $cek = $this->M_client->insert($data);
        }
            //var_dump($cek);
        $tb_client = $this->M_client->selectByIdUser($user_id)->row_array();

        if($cek){
            $this->response($tb_client, 200);
        }else{
            $this->response(array('status'=> 'fail', 502));
        }


    }

    function index_put(){
        $data= array(
            'macAddress' => $this->post('macAddress')
        );
        $this->db->where('macAddress', $username);
        $update= $this->db->update('tb_user', $data);

        if($update){
            $this->response($data, 200);
        }else{
            $this->response(array('status'=>'fail', 502));
        }

    }
}
?>