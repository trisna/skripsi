<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Home extends REST_Controller {

	function __construct() {
		parent::__construct();
		$this->load->database();

        $this->methods['index_get']['limit'] = 500; // 500 requests per hour per user/key

        //header('Content-Type: application/json');
    }

    //Menampilkan data rumah
   /* public function index_get() { 
        $data = $this->db->get('tb_rumah')->result();		
        $this->response(array("result"=>$data, 200));        
    }*/

     /////////// Generating Token and put user data into  token ///////////

    public function index_get($input)
    {
        if($input == 'token'){
            $data = $this->db->get('tb_rumah')->result();       
            $this->response(array("result"=>$data, 200));
        }

    }

    

}
?>