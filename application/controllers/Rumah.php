<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/implementJwt.php';

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Rumah extends REST_Controller {

	function __construct($config = 'rest') {
		parent::__construct($config);
		$this->load->database();

        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key

        $this->objOfJwt = new ImplementJwt();
        header('Content-Type: application/json');
    }

    //Menampilkan data rumah
   /* public function index_get() { 
        $data = $this->db->get('tb_rumah')->result();		
        $this->response(array("result"=>$data, 200));        
    }*/

     /////////// Generating Token and put user data into  token ///////////

    public function index_get($input, $token=NULL)
    {
        if($input == 'token'){            
            $tokenData['uniqueId'] = 'Trisna';
            $tokenData['role'] = 'admin';
            $tokenData['timeStamp'] = Date('Y-m-d h:i:s');
            $jwtToken = $this->objOfJwt->GenerateToken($tokenData);
            echo json_encode(array('Token'=>$jwtToken));
        }else if($input == 'gettoken'){
            $received_Token = $this->input->request_headers('Authorization');
            try
            {
            //var_dump($token);
                if($token != NULL){
                    $received_Token['Token'] = $token;
                    $jwtData = $this->objOfJwt->DecodeToken($token);
                }else{
                    $jwtData = $this->objOfJwt->DecodeToken($received_Token['Token']);
                }
                
                $data = $this->db->get('tb_rumah')->result();       
                $this->response(array("result"=>$data, 200));

            }
            catch (Exception $e)
            {
                http_response_code('401');
                echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
            }
        }else if($input == 'hasil'){
            $macAddress= $this->get('macAddress');
            $tb_user = $this->M_user->selectBymacAddress($macAddress)->row_array();

            $tb_hasil = $this->M_hasil->selectByIdUser($tb_user['id'])->row_array();

            $data = $this->M_rumah->selectById($tb_hasil['id_rumah'])->result();
            /*var_dump($data);*/
            $this->response($data, 200);
            //$data = $this->M_rumah->selectById('tb_rumah')->result();
        }
    }

    

}
?>