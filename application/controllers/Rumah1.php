<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Rumah1 extends CI_Controller {



    public function __construct(){
        parent::__construct();
        $this->load->database();

    }

    

     //Menampilkan data kontak
    public function komputasi() { 
        $kontak = $this->M_rumah->selectAll()->result_array();

        $max_luasTanah = $this->M_rumah->max('luas_tanah')->row_array();
        $max_luasBangunan = $this->M_rumah->max('luas_bangunan')->row_array();
        $max_lantaiRumah = $this->M_rumah->max('lantai_rumah')->row_array();
        $max_jumlahKamar = $this->M_rumah->max('jumlah_kamar')->row_array();


        $min_pendidikan = $this->M_rumah->min('pendidikan')->row_array();
        $min_rumah_sakit = $this->M_rumah->min('rumah_sakit')->row_array();
        $min_kantor_polisi = $this->M_rumah->min('kantor_polisi')->row_array();
        $min_pemadam_kebakaran = $this->M_rumah->min('pemadam_kebakaran')->row_array();
        $min_harga_rumah = $this->M_rumah->min('harga_rumah')->row_array();

        

        $data='';
        $i=0;
        foreach ($kontak as $key) {
            $data[$i]['luas_tanah'] = $key['luas_tanah']/$max_luasTanah['luas_tanah'];
            $data[$i]['luas_bangunan'] = $key['luas_bangunan']/$max_luasBangunan['luas_bangunan'];
            $data[$i]['lantai_rumah'] = $key['lantai_rumah']/$max_lantaiRumah['lantai_rumah'];
            $data[$i]['jumlah_kamar'] = $key['jumlah_kamar']/$max_jumlahKamar['jumlah_kamar'];


            $data[$i]['pendidikan'] = $min_pendidikan['pendidikan']/$key['pendidikan'];
            $data[$i]['rumah_sakit'] = $min_rumah_sakit['rumah_sakit']/$key['rumah_sakit'];
            $data[$i]['kantor_polisi'] = $min_kantor_polisi['kantor_polisi']/$key['kantor_polisi'];
            $data[$i]['pemadam_kebakaran'] = $min_pemadam_kebakaran['pemadam_kebakaran']/$key['pemadam_kebakaran'];
            $data[$i]['harga_rumah'] = $min_harga_rumah['harga_rumah']/$key['harga_rumah'];
            //echo $kontak['luas_tanah'].'///';

            $input['id_rumah'] = $key['id'];
            $input['luas_tanah'] = $data[$i]['luas_tanah'];
            $input['luas_bangunan'] = $data[$i]['luas_bangunan'];
            $input['lantai_rumah'] = $data[$i]['lantai_rumah'];
            $input['jumlah_kamar'] = $data[$i]['jumlah_kamar'];

            $input['pendidikan'] = $data[$i]['pendidikan'];
            $input['rumah_sakit'] = $data[$i]['rumah_sakit'];
            $input['kantor_polisi'] = $data[$i]['kantor_polisi'];
            $input['pemadam_kebakaran'] = $data[$i]['pemadam_kebakaran'];
            $input['harga_rumah'] = $data[$i]['harga_rumah'];
            $this->M_komputasi->insert($input);
            //var_dump($input);
            $i++;
            //$data['cek'] = $data;
        }

        //echo $max_luasTanah['luas_tanah'];
        //var_dump($data);
        //$this->load->view('rumah', $data);
        //$this->response(array("result"=>$kontak, 200));

        /*echo "<!DOCTYPE html>
        <html>
        <head>
        <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
      }

      td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
      }

      tr:nth-child(even) {
          background-color: #dddddd;
      }
      </style>
      </head>
      <body>

      <h2>Data Rumah</h2>
      <table>
      <tr>
      <th>No</th>
      <th>Luas Tanah</th>
      <th>Luas Bangunan</th>
      <th>Lantai Rumah</th>
      <th>Jumlah Kamar</th>
      <th>Dekat Dengan Pendidikan</th>
      <th>Dekat Dengan Rumah Sakit</th>
      <th>Dekat Dengan Kantor Polisi</th>
      <th>Dekat Dengan Pemadam Kebakaran</th>
      <th>Harga</th>
      </tr>
      ";
      $i=0;
      foreach ($kontak as $key) {   
        $data[$i]['luas_tanah'] = $key['luas_tanah']/$max_luasTanah['luas_tanah'];
        $data[$i]['luas_bangunan'] = $key['luas_bangunan']/$max_luasBangunan['luas_bangunan'];
        $data[$i]['lantai_rumah'] = $key['lantai_rumah']/$max_lantaiRumah['lantai_rumah'];
        $data[$i]['jumlah_kamar'] = $key['jumlah_kamar']/$max_jumlahKamar['jumlah_kamar'];

        $data[$i]['pendidikan'] = $min_pendidikan['pendidikan']/$key['pendidikan'];
        $data[$i]['rumah_sakit'] = $min_rumah_sakit['rumah_sakit']/$key['rumah_sakit'];
        $data[$i]['kantor_polisi'] = $min_kantor_polisi['kantor_polisi']/$key['kantor_polisi'];
        $data[$i]['pemadam_kebakaran'] = $min_pemadam_kebakaran['pemadam_kebakaran']/$key['pemadam_kebakaran'];
        $data[$i]['harga_rumah'] = $min_harga_rumah['harga_rumah']/$key['harga_rumah'];


        $temp1 = $data[$i]['luas_tanah'];
        $temp2 = $data[$i]['luas_bangunan'];
        $temp3 = $data[$i]['lantai_rumah'];
        $temp4 = $data[$i]['jumlah_kamar'];

        $temp5 = $data[$i]['pendidikan'];
        $temp6 = $data[$i]['rumah_sakit'];
        $temp7 = $data[$i]['kantor_polisi'];
        $temp8 = $data[$i]['pemadam_kebakaran'];
        $temp9 = $data[$i]['harga_rumah'];
            //echo $kontak['luas_tanah'].'///';
        $i++;     
        echo "
        <tr>
        <td>$i</td>
        <td>$temp1</td>
        <td>$temp2</td>
        <td>$temp3</td>
        <td>$temp4</td>          
        <td>$temp5</td>          
        <td>$temp6</td>          
        <td>$temp7</td>          
        <td>$temp8</td>          
        <td>$temp9</td>          
        </tr>
        ";
    }

    echo "
    </table>

    </body>
    </html>";*/
}


}
?>