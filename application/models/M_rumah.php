<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rumah extends CI_Model {

	public $tableName;

	public function __construct(){
		parent::__construct();
		$this->tableName = "tb_rumah";
	}

	public function selectAll($from=0,$offset=0){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->limit($from,$offset);

		return $this->db->get();
	}

	public function selectById($id){
		$this->db->select('*');
		$this->db->from($this->tableName);		
		$this->db->where('id',$id);
		
		// $this->db->limit($from,$offset);

		return $this->db->get();
	}

	public function max($input){
		$this->db->select('*');
		$this->db->from($this->tableName);	
		$this->db->order_by($input, "desc");		
        $this->db->limit(1);

		return $this->db->get();
	}

	public function min($input){
		$this->db->select('*');
		$this->db->from($this->tableName);	
		$this->db->order_by($input, "asc");		
        $this->db->limit(1);
        
		return $this->db->get();
	}
	
	public function insert($data){
		$this->db->insert($this->tableName,$data);

		return $this->db->insert_id();
	}
	
}