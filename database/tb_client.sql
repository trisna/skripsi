-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mei 2019 pada 18.26
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_komputasi`
--

CREATE TABLE `tb_client` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `luas_tanah` varchar(11) NOT NULL,
  `luas_bangunan` varchar(11) NOT NULL,
  `lantai_rumah` varchar(11) NOT NULL,
  `jumlah_kamar` varchar(11) NOT NULL,
  `pendidikan` varchar(11) NOT NULL,
  `rumah_sakit` varchar(11) NOT NULL,
  `kantor_polisi` varchar(11) NOT NULL,
  `pemadam_kebakaran` varchar(11) NOT NULL,
  `harga_rumah` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_komputasi`
--

