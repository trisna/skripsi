-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Mei 2019 pada 00.10
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_rumah`
--

CREATE TABLE `tb_rumah` (
  `id` int(11) NOT NULL,
  `luas_tanah` int(11) NOT NULL,
  `luas_bangunan` int(11) NOT NULL,
  `lantai_rumah` int(11) NOT NULL,
  `jumlah_kamar` int(11) NOT NULL,
  `pendidikan` int(11) NOT NULL,
  `rumah_sakit` int(11) NOT NULL,
  `kantor_polisi` int(11) NOT NULL,
  `pemadam_kebakaran` int(11) NOT NULL,
  `harga_rumah` bigint(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota` enum('bandung','jakarta','surabaya') NOT NULL,
  `provinsi` enum('Jawa Barat','Jawa Tengah','Jawa Timur') NOT NULL,
  `foto` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_rumah`
--

INSERT INTO `tb_rumah` (`id`, `luas_tanah`, `luas_bangunan`, `lantai_rumah`, `jumlah_kamar`, `pendidikan`, `rumah_sakit`, `kantor_polisi`, `pemadam_kebakaran`, `harga_rumah`, `alamat`, `kota`, `provinsi`, `foto`) VALUES
(1, 81, 33, 1, 2, 1, 1, 1, 1, 566000000, 'Rumah Luxury di Kawasan strategis Cibeunying Bandu', 'bandung', 'Jawa Barat', 'Rumah_Luxury_di_Kawasan_strategis_Cibeunying_Bandung.jpg'),
(2, 100, 36, 1, 2, 1, 1, 1, 1, 500000000, 'Jalan Permata Bumi Raya, Antapani', 'bandung', 'Jawa Barat', 'Jalan_Permata_Bumi_Raya,_Antapani-Bandung.jpg'),
(3, 72, 36, 1, 2, 1, 1, 1, 1, 275000000, 'Jl Pacuan Kuda, Sukamiskin, Arcamanik,', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(4, 60, 36, 1, 2, 1, 1, 1, 1, 300000000, 'Jl. Raya Ujung Berung, Ujung Berung', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(5, 60, 36, 1, 2, 1, 1, 1, 1, 276000000, 'Jalan Raya Laswi, Ciparay', 'bandung', 'Jawa Barat', 'Jalan_Raya_Laswi,_Ciparay-Bandung.jpg'),
(6, 355, 305, 2, 4, 1, 1, 1, 1, 6500000000, 'Komp Kumala Garden, Cibogo, Pasteur', 'bandung', 'Jawa Barat', 'Komp_Kumala_Garden,_Cibogo,_Pasteur-Bandung.jpg'),
(7, 72, 60, 2, 3, 1, 1, 1, 1, 691000000, 'Jl.Cigadung Raya Timur No.28, Cigadung', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(8, 72, 60, 2, 3, 1, 1, 1, 1, 679000000, 'Cilengkrang, Bandung Timur', 'bandung', 'Jawa Barat', 'Cilengkrang,_Bandung_Timur-Bandung.jpg'),
(9, 60, 30, 1, 2, 1, 1, 1, 1, 447000000, 'Jl.Ciwastra, Bandung Timur', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(10, 72, 60, 2, 3, 1, 1, 1, 1, 678000000, 'Jl. Alam Raya Ciwastra, Bandung Timur', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(11, 107, 60, 2, 3, 1, 1, 1, 1, 820000000, 'TERUSAN BUAH BATU, Bandung Timur', 'bandung', 'Jawa Barat', 'TERUSAN_BUAH_BATU,_Bandung_Timur-Bandung.jpg'),
(12, 84, 45, 1, 2, 1, 1, 1, 1, 645000000, 'Jl.Alam Raya, Bandung Timur', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(13, 65, 36, 1, 2, 1, 1, 1, 1, 468000000, 'CIPOREAT, Bandung Timur', 'bandung', 'Jawa Barat', 'CIPOREAT,_Bandung_Timur-Bandung.jpg'),
(14, 114, 36, 1, 2, 1, 1, 1, 1, 677000000, 'JATIHANDAP, Bandung Timur', 'bandung', 'Jawa Barat', 'JATIHANDAP,_Bandung_Timur-Bandung.jpg'),
(15, 850, 2000, 3, 68, 1, 1, 1, 1, 2500000000, 'Tubagus Ismail Bandung, Dago', 'bandung', 'Jawa Barat', 'Tubagus_Ismail_Bandung,_Dago-Bandung.jpg'),
(16, 906, 632, 2, 10, 1, 1, 1, 1, 2900000000, 'Sayap Dipati Ukur-Dago Bandung, H. Juanda Dago', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(17, 70, 45, 1, 2, 1, 1, 1, 1, 399000000, 'Jl. Babakan Batawi, Ujung Berung', 'bandung', 'Jawa Barat', 'Jl_Pacuan_Kuda,_Sukamiskin,_Arcamanik,-Bandung.jpg'),
(18, 72, 36, 1, 2, 1, 1, 1, 1, 361800000, 'Katapang Kopo', 'bandung', 'Jawa Barat', 'Katapang_Kopo-Bandung.jpg'),
(19, 84, 48, 1, 2, 1, 1, 1, 1, 442200000, 'Katapang Kopo', 'bandung', 'Jawa Barat', 'Katapang_Kopo-Bandung1.jpg'),
(20, 96, 53, 1, 2, 1, 1, 1, 1, 499150000, 'Katapang Kopo', 'bandung', 'Jawa Barat', 'Katapang_Kopo-Bandung2.jpg'),
(21, 60, 40, 1, 2, 1, 1, 1, 1, 314000000, 'Katapang Kopo', 'bandung', 'Jawa Barat', 'Katapang_Kopo-Bandung3.jpg'),
(22, 88, 69, 2, 3, 1, 1, 1, 1, 800000000, 'Cimindi', 'bandung', 'Jawa Barat', 'Cimindi-Bandung.jpg'),
(23, 50, 30, 1, 2, 1, 1, 1, 1, 140000000, 'Sukamukti Katapang Bandung Selatan', 'bandung', 'Jawa Barat', 'Sukamukti_Katapang_Bandung_Selatan-Bandung.jpg'),
(24, 50, 30, 1, 2, 1, 1, 1, 1, 135000000, 'JL Sangkahurip-Suka Mukti Katapang', 'bandung', 'Jawa Barat', 'JL_Sangkahurip-Suka_Mukti_Katapang-Bandung.jpg'),
(25, 50, 30, 1, 2, 1, 1, 1, 1, 100000000, 'Jl Terusan Andir-Katapang Kab Bandung', 'bandung', 'Jawa Barat', 'Jl_Terusan_Andir-Katapang_Kab_Bandung-Bandung.jpg'),
(26, 78, 62, 2, 3, 1, 1, 1, 1, 560000000, 'Jl Cigugur, Parongpong, Bandung Utara', 'bandung', 'Jawa Barat', 'Jl_Cigugur,_Parongpong,_Bandung_Utara-Bandung.jpg'),
(27, 75, 45, 1, 2, 1, 1, 1, 1, 540000000, 'Cipalago, Bojongsoang, Buahbatu', 'bandung', 'Jawa Barat', 'Cipalago,_Bojongsoang,_Buahbatu-Bandung.jpg'),
(28, 72, 36, 1, 2, 1, 1, 1, 1, 617000000, 'JL. JATIHANDAP, Bandung Timur', 'bandung', 'Jawa Barat', 'null.png'),
(29, 72, 60, 2, 3, 1, 1, 1, 1, 696000000, 'CITRA GREEN, Cimenyan', 'bandung', 'Jawa Barat', 'CITRA_GREEN,_Cimenyan-Bandung.jpg'),
(30, 800, 500, 1, 3, 1, 1, 1, 1, 35000000000, 'Hegarmanah, Setiabudi', 'bandung', 'Jawa Barat', 'Hegarmanah,_Setiabudi-Bandung.jpg'),
(31, 803, 536, 1, 4, 1, 1, 1, 1, 14000000000, 'Komplek Budi Asih-Setiabudi', 'bandung', 'Jawa Barat', 'Komplek_Budi_Asih-Setiabudi-Bandung.jpg'),
(32, 560, 350, 1, 2, 1, 1, 1, 1, 12000000000, 'Karang Tinggal-Sukajadi-PVJ', 'bandung', 'Jawa Barat', 'Karang_Tinggal-Sukajadi-PVJ-Bandung.jpg'),
(33, 1080, 650, 3, 6, 1, 1, 1, 1, 8500000000, 'Komplek Pondok Hijau Indah Bandung, Geger Kalong', 'bandung', 'Jawa Barat', 'Komplek_Pondok_Hijau_Indah_Bandung,_Geger_Kalong-Bandung.jpg'),
(34, 799, 600, 1, 9, 1, 1, 1, 1, 25000000000, 'Jl.Bengawan Anggrek Sayap Riau RE Martadinata', 'bandung', 'Jawa Barat', 'null.png'),
(35, 830, 400, 1, 4, 1, 1, 1, 1, 34000000000, 'Jalan Raya Supratman Bandung', 'bandung', 'Jawa Barat', 'Jalan_Raya_Supratman_Bandung-Bandung.jpg'),
(36, 2454, 4000, 4, 100, 1, 1, 1, 1, 49000000000, 'Cibogo-Belakang Univ.Maranatha Dan Tol Pasteur', 'bandung', 'Jawa Barat', 'null.png'),
(37, 700, 550, 1, 5, 1, 1, 1, 1, 6900000000, 'Rancakendal_Dago Resort', 'bandung', 'Jawa Barat', 'Rancakendal_Dago_Resort-Bandung.jpg'),
(38, 490, 300, 1, 3, 1, 1, 1, 1, 4900000000, 'Dago Pakar Resort Bandung', 'bandung', 'Jawa Barat', 'Dago_Pakar_Resort_Bandung-Bandung.jpg'),
(39, 450, 400, 1, 6, 1, 1, 1, 1, 5200000000, 'Di Belakang Setrasari Mall', 'bandung', 'Jawa Barat', 'Di_Belakang_Setrasari_Mall-Bandung.jpg'),
(40, 900, 1700, 3, 51, 1, 1, 1, 1, 16000000000, 'H. Juanda Dago', 'bandung', 'Jawa Barat', 'null.png'),
(41, 135, 130, 2, 4, 1, 1, 1, 1, 1150000000, 'Ciwastra', 'bandung', 'Jawa Barat', 'Ciwastra-Bandung.jpg'),
(42, 157, 47, 1, 2, 1, 1, 1, 1, 510000000, 'Jl Cipamokolan, Cisaranten Kidul, Gedebage', 'bandung', 'Jawa Barat', 'Jl_Cipamokolan,_Cisaranten_Kidul,_Gedebage-Bandung.jpg'),
(43, 112, 47, 1, 2, 1, 1, 1, 1, 585000000, 'Jl. Cisaranten Kulon, Arcamanik', 'bandung', 'Jawa Barat', 'null.png'),
(44, 730, 600, 4, 28, 1, 1, 1, 1, 8500000000, 'Tubagus Ismail-Dago', 'bandung', 'Jawa Barat', 'Tubagus_Ismail-Dago-Bandung.jpg'),
(45, 500, 530, 2, 4, 1, 1, 1, 1, 7750000000, 'Komplek Citra Green Dago', 'bandung', 'Jawa Barat', 'Komplek_Citra_Green_Dago-Bandung.jpg'),
(46, 800, 677, 2, 4, 1, 1, 1, 1, 10500000000, 'Komplek Dago Pakar Resort', 'bandung', 'Jawa Barat', 'Komplek_Dago_Pakar_Resort-Bandung.jpg'),
(47, 270, 164, 1, 3, 1, 1, 1, 1, 2900000000, 'Pasteur', 'bandung', 'Jawa Barat', 'Pasteur-Bandung.jpg'),
(48, 300, 550, 2, 4, 1, 1, 1, 1, 4500000000, 'Bukit Ligar-Cigadung-Dago Pakar Resort', 'bandung', 'Jawa Barat', 'Bukit_Ligar-Cigadung-Dago_Pakar_Resort-Bandung.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_rumah`
--
ALTER TABLE `tb_rumah`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_rumah`
--
ALTER TABLE `tb_rumah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
